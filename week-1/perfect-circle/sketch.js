const middle = 200;
const radius = 50;
let vec;

function setup() {
  createCanvas(400, 400);
  vec = createVector();
  background(220);
  drawCircle();
}

function drawCircle() {
  let x = radius;
  let y = 0;
  while(y <= 35) {
    vec.set(x, y);
    if (vec.mag() >= radius) {
      x--;
    }
    drawMirrored(x, y);
    drawMirrored(y, x);
    y++;
  }


}

function drawMirrored(pointX, pointY) {
  point(pointX + middle, pointY + middle)
  point(pointX + middle, -pointY + middle)
  point(-pointX + middle, pointY + middle)
  point(-pointX + middle, -pointY + middle)
}