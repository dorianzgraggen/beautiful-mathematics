var size_can = 800;
var mid_point;
var i = 0;

function setup() {
  createCanvas(size_can, size_can);
  background(color(0));

  mid_point = createVector(width/2,height/2);
}

function draw() {

  drawSpiral(100, color(255, 0, 0), i + 120)
  drawSpiral(100, color(0, 255, 0), i + 240)
  drawSpiral(100, color(0, 0, 255), i + 360)

  updatePixels();  
  
  if (i < 360) i++;
  else i = 0;
}

function drawSpiral(speed, color, startAngle) {
  let rad = 0;
  for (let j = 0; j < 6; j++) {
    for (let a=startAngle; a<=360+startAngle; a++)
    {
        rad += speed/360;
        let x = rad*sin(radians(a))+mid_point.x;
        let y = rad*cos(radians(a))+mid_point.y;
        if (x > width|| x < -1 || y > height || y < 0) {
          continue;
        }
        set(x,y,color);
    }
  }
}
