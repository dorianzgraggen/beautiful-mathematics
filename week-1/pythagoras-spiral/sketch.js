var size_can = 800;
var margin = 50;
var mid_point;

function setup() {
  let bk_col = color(0)

  createCanvas(size_can, size_can);
  background(bk_col);

  mid_point = createVector(width/2,height/2);

  drawSpiral(100, color(255, 0, 0), 120)
  drawSpiral(100, color(0, 255, 0), 240)
  drawSpiral(100, color(0, 0, 255), 360)

  updatePixels();  
}

function drawSpiral(speed, color, startAngle) {
  let rad = 0;
  for (let i = 0; i < 20; i++) {
    for (let a=startAngle; a<=360+startAngle; a++)
    {
        rad += speed/360;
        let x = rad*sin(radians(a))+mid_point.x;
        let y = rad*cos(radians(a))+mid_point.y;
        set(x,y,color);
    }
  }
}
