let vec;

function setup() {
  angleMode(DEGREES)

  vec = createVector(0, 0);
  createCanvas(400, 400);
  background(220);
  drawSpiral(100, 100);
  drawSpiral(100, 300);
  drawSpiral(300, 300);
  drawSpiral(300, 100);
}

function drawSpiral(centerX, centerY) {
  vec.set(1, 1);
  for(let i = 0; i < 360; i++) {
    vec.add(0.25, 0.1);
    let vecSpiral = vec.copy();
    vecSpiral.rotate(i);
    point(vecSpiral.x + centerX, vecSpiral.y + centerY);
  }
}
