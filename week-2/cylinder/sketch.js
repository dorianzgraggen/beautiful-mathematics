const rad = 100;
const height = -300;
let circlePoints = [];

function setup() {
  createCanvas(900, 500, WEBGL);
  calcCirclePoints();
}

function draw() {
  clear();
  drawCylinder();
  orbitControl();
}

function calcCirclePoints() {
  for (let a=0; a<=360; a+=10)
  {
    let x = rad*sin(radians(a));
    let y = rad*cos(radians(a));
    circlePoints.push({x: x, y: y, z: 0});
    circlePoints.push({x: x, y: y, z: height});
  }
}

function drawCylinder() {
  beginShape(TRIANGLE_STRIP);
  circlePoints.forEach((point, index) => {
    vertex(point.x, point.y, point.z)
  });
  endShape();
}