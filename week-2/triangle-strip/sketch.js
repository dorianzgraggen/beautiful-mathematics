function setup() {
  createCanvas(400, 400, WEBGL);
}

function draw() {
  clear();
  createShape();
  orbitControl();
}

function createShape() {
  let z = 0;
  let width = 50;

  beginShape(TRIANGLE_STRIP);
  for (let i = 0; i < 20; i++) {
    vertex(width * Math.floor(i / 2), width * (i % 2), z);
  }
  endShape();
}
