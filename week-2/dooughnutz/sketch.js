const rad = 100;
const height = -300;
const circlePoints = [];

function setup() {
  createCanvas(900, 500, WEBGL);
  angleMode(DEGREES);
  calcCirclePoints();
}

function draw() {
  clear();
  drawDognut();
  orbitControl();
}

function calcCirclePoints() {
  let firstCirclePoints = [];
  for (let a=0; a<=360; a+=10)
  {
    let x = rad*sin(a);
    let y = rad*cos(a);
    firstCirclePoints.push({x: 2* rad + x, y: y, z: 0});
  }
  circlePoints.push(firstCirclePoints);

  for (let a=10; a<=360; a+=10) {
    let currentCirclePoints = [];
    firstCirclePoints.forEach((point) => {
      currentCirclePoints.push(rotatePoint(point, a));
    })
    circlePoints.push(currentCirclePoints);
  }
  circlePoints.push(firstCirclePoints);
}

function drawDognut() {
  beginShape(TRIANGLE_STRIP);
  for (let i = 0; i < circlePoints.length - 1; i++)
  for (let j = 0; j < circlePoints[0].length; j++) {
    const firstPoint = circlePoints[i][j];
    const secondPoint = circlePoints[i + 1][j];
    vertex(firstPoint.x, firstPoint.y, firstPoint.z);
    vertex(secondPoint.x, secondPoint.y, secondPoint.z);
  }
  endShape();
}

function rotatePoint(p, a) {
  let newPoint = {
    x: cos(a) * p.x + sin(a) * p.z,
    y: p.y,
    z: -sin(a) * p.x + cos(a) * p.z
  }
  return newPoint;
}